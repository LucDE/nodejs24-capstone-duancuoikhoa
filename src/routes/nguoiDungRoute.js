const express = require("express");
const {
  getUsers,
  getUserById,
  postUser,
  updateUser,
  deleteUser,
  getUserByName,
} = require("../controllers/nguoiDungController");

const nguoiDungRoute = express.Router();

nguoiDungRoute.get("/users", getUsers);

nguoiDungRoute.get("/users/:id", getUserById);

nguoiDungRoute.post("/users", postUser);

nguoiDungRoute.put("/users", updateUser);

nguoiDungRoute.delete("/users/:id", deleteUser);

nguoiDungRoute.get("/users/getUserByName/:userName", getUserByName);

module.exports = nguoiDungRoute;
