const express = require("express");
const {
  getThueCongViec,
  getThueCongViecById,
  postThueCongViec,
  updateThueCongViec,
  deleteThueCongViec,
  getDanhSachCongViecDaThue,
  postHoanThanhCongViec,
} = require("../controllers/thueCongViecController");

const thueCongViecRoute = express.Router();

thueCongViecRoute.get("/thue-cong-viec", getThueCongViec);

thueCongViecRoute.post("/thue-cong-viec", postThueCongViec);

thueCongViecRoute.get("/thue-cong-viec/:id", getThueCongViecById);

thueCongViecRoute.put("/thue-cong-viec", updateThueCongViec);

thueCongViecRoute.delete("/thue-cong-viec/:id", deleteThueCongViec);

thueCongViecRoute.get("/cong-viec-da-thue", getDanhSachCongViecDaThue);

thueCongViecRoute.post(
  "/thue-cong-viec/hoan-thanh-cong-viec/:id",
  postHoanThanhCongViec
);

module.exports = thueCongViecRoute;
