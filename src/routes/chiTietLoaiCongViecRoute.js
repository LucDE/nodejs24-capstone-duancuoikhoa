const express = require("express");

const {
  getChiTietLoaiCongViec,
  addChiTietLoaiCongViec,
  getChiTietLoaiCongViecById,
  updateChiTietLoaiCongViec,
  deleteChiTietLoaiCongViec,
  getChiTietLoaiCongViecPhanTrang,
} = require("../controllers/chiTietLoaiCongViecController");
const chiTietLoaiCongViecRoute = express.Router();

chiTietLoaiCongViecRoute.get(
  "/chi-tiet-loai-cong-viec",
  getChiTietLoaiCongViec
);

chiTietLoaiCongViecRoute.post(
  "/chi-tiet-loai-cong-viec",
  addChiTietLoaiCongViec
);

chiTietLoaiCongViecRoute.get(
  "/chi-tiet-loai-cong-viec/:id",
  getChiTietLoaiCongViecById
);

chiTietLoaiCongViecRoute.put(
  "/chi-tiet-loai-cong-viec",
  updateChiTietLoaiCongViec
);

chiTietLoaiCongViecRoute.delete(
  "/chi-tiet-loai-cong-viec/:id",
  deleteChiTietLoaiCongViec
);

chiTietLoaiCongViecRoute.get(
  "/chi-tiet-loai-cong-viec/phan-trang-tim-kiem/:pageIndex/:pageSize",
  getChiTietLoaiCongViecPhanTrang
);

module.exports = chiTietLoaiCongViecRoute;
