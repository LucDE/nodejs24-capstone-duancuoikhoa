const express = require("express");
const {
  getBinhLuan,
  getBinhLuanById,
  postBinhLuan,
  updateBinhLuan,
  deleteBinhLuan,
  getBinhLuanTheoMaCongViec,
} = require("../controllers/binhLuanController");

const binhLuanRoute = express.Router();

binhLuanRoute.get("/binh-luan", getBinhLuan);

binhLuanRoute.post("/binh-luan", postBinhLuan);

binhLuanRoute.get("/binh-luan/:id", getBinhLuanById);

binhLuanRoute.put("/binh-luan", updateBinhLuan);

binhLuanRoute.delete("/binh-luan/:id", deleteBinhLuan);

binhLuanRoute.get(
  "/binh-luan/lay-binh-luan-theo-cong-viec/:maCongViec",
  getBinhLuanTheoMaCongViec
);

module.exports = binhLuanRoute;
