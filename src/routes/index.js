const express = require("express");
const rootRoute = express.Router();

const chiTietLoaiCongViecRoute = require("./chiTietLoaiCongViecRoute");
rootRoute.use(chiTietLoaiCongViecRoute);

const congViecRoute = require("./congViecRoute");
rootRoute.use(congViecRoute);

const loaiCongViecRoute = require("./loaiCongViecRoute");
rootRoute.use(loaiCongViecRoute);

const nguoiDungRoute = require("./nguoiDungRoute");
rootRoute.use(nguoiDungRoute);

const thueCongViecRoute = require("./ThueCongViecRoute");
rootRoute.use(thueCongViecRoute);

const binhLuanRoute = require("./binhLuanRoute");
rootRoute.use(binhLuanRoute);

module.exports = rootRoute;
