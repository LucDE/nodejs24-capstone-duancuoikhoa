const express = require("express");
const {
  getCongViec,
  getCongViecById,
  postCongViec,
  updateCongViec,
  deleteCongViec,
  getChiTietLoaiCongViec,
  getDanhSachCongViecTheoTen,
} = require("../controllers/congViecController");
const congViecRoute = express.Router();

congViecRoute.get("/cong-viec", getCongViec);

congViecRoute.post("/cong-viec", postCongViec);

congViecRoute.get("/cong-viec/:id", getCongViecById);

congViecRoute.put("/cong-viec", updateCongViec);

congViecRoute.delete("/cong-viec/:id", deleteCongViec);

congViecRoute.get(
  "/cong-viec/lay-chi-tiet-loai-cong-viec/:id",
  getChiTietLoaiCongViec
);

congViecRoute.get(
  "/cong-viec/lay-danh-sach-cong-viec-theo-ten/:tenCongViec",
  getDanhSachCongViecTheoTen
);

module.exports = congViecRoute;
