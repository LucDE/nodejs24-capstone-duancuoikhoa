const express = require("express");
const {
  getLoaiCongViec,
  postLoaiCongViec,
  getLoaiCongViecById,
  updateLoaiCongViec,
  deleteLoaiCongViec,
} = require("../controllers/loaiCongViecController");
const loaiCongViecRoute = express.Router();

loaiCongViecRoute.get("/loai-cong-viec", getLoaiCongViec);
loaiCongViecRoute.post("/loai-cong-viec", postLoaiCongViec);
loaiCongViecRoute.get("/loai-cong-viec/:id", getLoaiCongViecById);
loaiCongViecRoute.put("/loai-cong-viec", updateLoaiCongViec);
loaiCongViecRoute.delete("/loai-cong-viec/:id", deleteLoaiCongViec);

module.exports = loaiCongViecRoute;
