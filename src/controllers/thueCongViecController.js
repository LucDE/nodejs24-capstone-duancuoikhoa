const { PrismaClient } = require("@prisma/client");
const { successCode, failCode, errorCode } = require("../ultis/response");
const prisma = new PrismaClient();

const getThueCongViec = async (req, res) => {
  try {
    let result = await prisma.thueCongViec.findMany();
    successCode(res, result, "Lấy danh sách thuê công việc thành công !");
  } catch (err) {
    errorCode(res, "Lấy danh sách thất bại !");
  }
};

const getThueCongViecById = async (req, res) => {
  try {
    let { id } = req.params;
    let result = await prisma.thueCongViec.findFirst({
      where: {
        id: Number(id),
      },
    });
    if (result) {
      successCode(res, result, "Lấy thuê công việc thành công !");
    } else {
      failCode(res, result, "Không tìm thấy thuê công việc !");
    }
  } catch (err) {
    errorCode(res, "Lỗi BE");
  }
};

const postThueCongViec = async (req, res) => {
  try {
    let { ma_cong_viec, ma_nguoi_thue, hoan_thanh } = req.body;
    let checkNguoiThue = await prisma.nguoiDung.findFirst({
      where: {
        id: Number(ma_nguoi_thue),
      },
    });
    let checkCongViec = await prisma.congViec.findFirst({
      where: {
        id: Number(ma_cong_viec),
      },
    });
    if (!checkCongViec) {
      failCode(res, ma_cong_viec, "Không tồn tại công việc");
      return;
    }
    if (!checkNguoiThue) {
      failCode(res, ma_nguoi_thue, "Không tồn tại người thuê");
      return;
    }
    let data = {
      ma_cong_viec,
      ma_nguoi_thue,
      ngay_thue: new Date(),
      hoan_thanh,
    };
    let result = await prisma.thueCongViec.create({ data });
    successCode(res, result, "Thêm thuê công việc thành công!");
  } catch (err) {
    console.log("err: ", err);
    errorCode(res, "Lỗi BE");
  }
};

const updateThueCongViec = async (req, res) => {
  try {
    let { id, ma_cong_viec, ma_nguoi_thue, hoan_thanh } = req.body;
    let checkId = await prisma.thueCongViec.findFirst({
      where: {
        id: Number(id),
      },
    });
    if (!checkId) {
      failCode(res, id, "Không tìm thấy thuê công việc !");
      return;
    } else {
      let checkNguoiThue = await prisma.nguoiDung.findFirst({
        where: {
          id: Number(ma_nguoi_thue),
        },
      });
      let checkCongViec = await prisma.congViec.findFirst({
        where: {
          id: Number(ma_cong_viec),
        },
      });
      if (!checkCongViec) {
        failCode(res, ma_cong_viec, "Không tồn tại công việc");
        return;
      }
      if (!checkNguoiThue) {
        failCode(res, ma_nguoi_thue, "Không tồn tại người thuê");
        return;
      }
      let data = {
        id,
        ma_cong_viec,
        ma_nguoi_thue,
        ngay_thue: new Date(),
        hoan_thanh,
      };
      let result = await prisma.thueCongViec.update({
        data,
        where: {
          id,
        },
      });
      successCode(res, result, "Update thuê công việc thành công !");
    }
  } catch (err) {
    console.log("err: ", err);
    errorCode(res, "Lỗi BE");
  }
};

const deleteThueCongViec = async (req, res) => {
  try {
    let { id } = req.params;
    let checkId = await prisma.thueCongViec.findFirst({
      where: {
        id: Number(id),
      },
    });
    if (!checkId) {
      failCode(res, id, "Không tìm thấy thuê công việc cần xóa !");
      return;
    }
    let result = await prisma.thueCongViec.delete({
      where: { id: Number(id) },
    });
    successCode(res, result, "Xóa thành công !");
  } catch (err) {
    console.log("err: ", err);
    errorCode(res, "Lỗi BE");
  }
};

const getDanhSachCongViecDaThue = async (req, res) => {
  try {
    let result = await prisma.thueCongViec.findMany({
      include: {
        CongViec: true,
      },
    });

    let arrays = [];
    await result.forEach((item) => {
      arrays.push(item["CongViec"]);
    });

    successCode(res, arrays, "Lấy danh sách công việc đã thuê thành công !");
  } catch (err) {
    console.log("err: ", err);
    errorCode(res, "Lỗi BE");
  }
};

const postHoanThanhCongViec = async (req, res) => {
  let { id } = req.params;
  let result = await prisma.thueCongViec.findFirst({
    where: {
      id: Number(id),
    },
  });
  console.log("result: ", result);
  if (!result) {
    failCode(res, id, "Không tồn tại thuê công việc !");
    return;
  }
  if (result.hoan_thanh) {
    failCode(res, id, "Công việc đã hoàn thành trước đó !");
    return;
  }
  let data = { ...result, hoan_thanh: true };
  let newResult = await prisma.thueCongViec.update({
    data,
    where: {
      id: Number(id),
    },
  });
  successCode(res, newResult, "Update thuê công việc thành công !");
};

//phân trang

module.exports = {
  getThueCongViec,
  getThueCongViecById,
  postThueCongViec,
  updateThueCongViec,
  deleteThueCongViec,
  getDanhSachCongViecDaThue,
  postHoanThanhCongViec,
};
