const { PrismaClient } = require("@prisma/client");
const { successCode, failCode, errorCode } = require("../ultis/response");
const prisma = new PrismaClient();

const getCongViec = async (req, res) => {
  try {
    let result = await prisma.congViec.findMany();
    successCode(res, result, "Lấy danh sách công việc thành công !");
  } catch (err) {
    errorCode(res, "Lấy danh sách thất bại !");
  }
};
const postCongViec = async (req, res) => {
  try {
    let {
      ten_cong_viec,
      danh_gia,
      gia_tien,
      hinh_anh,
      mo_ta,
      mo_ta_ngan,
      sao_cong_viec,
      ma_chi_tiet_loai,
      nguoi_tao,
    } = req.body;
    let checkNguoiTao = await prisma.nguoiDung.findFirst({
      where: {
        id: Number(nguoi_tao),
      },
    });
    let checkMaChiTietLoaiCongViec = await prisma.chiTietLoaiCongViec.findFirst(
      {
        where: {
          id: Number(ma_chi_tiet_loai),
        },
      }
    );
    if (!checkMaChiTietLoaiCongViec) {
      failCode(res, ma_chi_tiet_loai, "Không tồn tại chi tiết công việc");
      return;
    }
    if (!checkNguoiTao) {
      failCode(res, nguoi_tao, "Không tồn tại người tạo");
      return;
    }
    let data = {
      ten_cong_viec,
      danh_gia,
      gia_tien,
      hinh_anh,
      mo_ta,
      mo_ta_ngan,
      sao_cong_viec,
      ma_chi_tiet_loai,
      nguoi_tao,
    };
    let result = await prisma.congViec.create({ data });
    successCode(res, result, "Thêm công việc thành công!");
  } catch (err) {
    console.log("err: ", err);
    errorCode(res, "Lỗi BE");
  }
};

const getCongViecById = async (req, res) => {
  try {
    let { id } = req.params;
    let result = await prisma.congViec.findFirst({
      where: {
        id: Number(id),
      },
    });
    if (result) {
      successCode(res, result, "Lấy công việc thành công !");
    } else {
      failCode(res, result, "Không tìm thấy công việc !");
    }
  } catch (err) {
    errorCode(res, "Lỗi BE");
  }
};

const updateCongViec = async (req, res) => {
  try {
    let {
      id,
      ten_cong_viec,
      danh_gia,
      gia_tien,
      hinh_anh,
      mo_ta,
      mo_ta_ngan,
      sao_cong_viec,
      ma_chi_tiet_loai,
      nguoi_tao,
    } = req.body;
    let checkId = await prisma.congViec.findFirst({
      where: {
        id: Number(id),
      },
    });
    if (!checkId) {
      failCode(res, id, "Không tìm thấy công việc !");
      return;
    } else {
      let checkNguoiTao = await prisma.nguoiDung.findFirst({
        where: {
          id: Number(nguoi_tao),
        },
      });
      let checkMaChiTietLoaiCongViec =
        await prisma.chiTietLoaiCongViec.findFirst({
          where: {
            id: Number(ma_chi_tiet_loai),
          },
        });
      if (!checkMaChiTietLoaiCongViec) {
        failCode(res, ma_chi_tiet_loai, "Không tồn tại chi tiết công việc");
        return;
      }
      if (!checkNguoiTao) {
        failCode(res, nguoi_tao, "Không tồn tại người tạo");
        return;
      }
      let data = {
        id,
        ten_cong_viec,
        danh_gia,
        gia_tien,
        hinh_anh,
        mo_ta,
        mo_ta_ngan,
        sao_cong_viec,
        ma_chi_tiet_loai,
        nguoi_tao,
      };
      let result = await prisma.congViec.update({
        data,
        where: {
          id,
        },
      });
      successCode(res, result, "Update công việc thành công !");
    }
  } catch (err) {
    console.log("err: ", err);
    errorCode(res, "Lỗi BE");
  }
};

const deleteCongViec = async (req, res) => {
  try {
    let { id } = req.params;
    let checkId = await prisma.congViec.findFirst({
      where: {
        id: Number(id),
      },
    });
    if (!checkId) {
      failCode(res, id, "Không tìm thấy công việc cần xóa !");
      return;
    }
    let result = await prisma.congViec.delete({
      where: { id: Number(id) },
    });
    successCode(res, result, "Xóa thành công !");
  } catch (err) {
    console.log("err: ", err);
    errorCode(res, "Lỗi BE");
  }
};

const getChiTietLoaiCongViec = async (req, res) => {
  try {
    let { id } = req.params;
    let checkMaChiTietLoaiCongViec = await prisma.congViec.findFirst({
      where: {
        ma_chi_tiet_loai: Number(id),
      },
    });
    if (!checkMaChiTietLoaiCongViec) {
      failCode(res, id, "Không tồn tại chi tiết công việc");
      return;
    }
    let result = await prisma.congViec.findMany({
      include: {
        ChiTietLoaiCongViec: true,
      },
      where: {
        ma_chi_tiet_loai: Number(id),
      },
    });
    successCode(res, result, "Lấy chi tiết loại công việc thành công !");
  } catch (err) {
    console.log("err: ", err);
    errorCode(res, "Lỗi BE");
  }
};

let getDanhSachCongViecTheoTen = async (req, res) => {
  try {
    let { tenCongViec } = req.params;
    let result = await prisma.congViec.findMany({
      where: {
        ten_cong_viec: {},
      },
    });
    if (result.length === 0) {
      failCode(res, "", "Không tìm thấy tên");
      return;
    }
    successCode(res, result, "Tìm thành công !");
  } catch (err) {
    console.log("err: ", err);
    errorCode(res, "Lỗi BE");
  }
};

module.exports = {
  getCongViec,
  getCongViecById,
  postCongViec,
  updateCongViec,
  deleteCongViec,
  getChiTietLoaiCongViec,
  getDanhSachCongViecTheoTen,
};
