const { PrismaClient } = require("@prisma/client");
const { successCode, failCode, errorCode } = require("../ultis/response");
const prisma = new PrismaClient();

const getUsers = async (req, res) => {
  try {
    let result = await prisma.nguoiDung.findMany();
    successCode(res, result, "Lấy danh sách người dùng thành công !");
  } catch (err) {
    errorCode(res, "Lấy danh sách thất bại !");
  }
};

const getUserById = async (req, res) => {
  try {
    let { id } = req.params;
    let result = await prisma.nguoiDung.findFirst({
      where: {
        id: Number(id),
      },
    });
    if (result) {
      successCode(res, result, "Lấy user thành công !");
    } else {
      failCode(res, result, "Không tìm thấy user !");
    }
  } catch (err) {
    errorCode(res, "Lỗi BE");
  }
};

const postUser = async (req, res) => {
  try {
    let {
      name,
      email,
      pass_word,
      phone,
      birth_day,
      gender,
      role,
      skill,
      certification,
    } = req.body;
    let checkEmail = await prisma.nguoiDung.findFirst({
      where: {
        email,
      },
    });
    if (checkEmail) {
      failCode(res, email, "Email đã tồn tại !");
      return;
    }
    let data = {
      name,
      email,
      pass_word,
      phone,
      birth_day,
      gender,
      role,
      skill,
      certification,
    };
    let result = await prisma.nguoiDung.create({ data });
    successCode(res, result, "Thêm user thành công!");
  } catch (err) {
    console.log("err: ", err);
    errorCode(res, "Lỗi BE");
  }
};

const updateUser = async (req, res) => {
  try {
    let {
      id,
      name,
      email,
      pass_word,
      phone,
      birth_day,
      gender,
      role,
      skill,
      certification,
    } = req.body;
    let checkId = await prisma.nguoiDung.findFirst({
      where: {
        id: Number(id),
      },
    });
    if (!checkId) {
      failCode(res, id, "Không tìm thấy người dùng !");
      return;
    }
    let data = {
      id,
      name,
      email,
      pass_word,
      phone,
      birth_day,
      gender,
      role,
      skill,
      certification,
    };
    let result = await prisma.nguoiDung.update({
      data,
      where: {
        id,
      },
    });
    successCode(res, result, "Update User thành công !");
  } catch (err) {
    console.log("err: ", err);
    errorCode(res, "Lỗi BE");
  }
};

const deleteUser = async (req, res) => {
  try {
    let { id } = req.params;
    let checkId = await prisma.nguoiDung.findFirst({
      where: {
        id: Number(id),
      },
    });
    if (!checkId) {
      failCode(res, id, "Không tìm thấy người dùng cần xóa !");
      return;
    }
    let result = await prisma.nguoiDung.delete({
      where: { id: Number(id) },
    });
    successCode(res, result, "Xóa thành công !");
  } catch (err) {
    console.log("err: ", err);
    errorCode(res, "Lỗi BE");
  }
};

let getUserByName = async (req, res) => {
  try {
    let { userName } = req.params;
    let result = await prisma.nguoiDung.findMany({
      where: {
        name: {
          contains: userName,
        },
      },
    });
    if (result.length === 0) {
      failCode(res, "", "Không tìm thấy tên");
      return;
    }
    successCode(res, result, "Tìm thành công !");
  } catch (err) {
    console.log("err: ", err);
    errorCode(res, "Lỗi BE");
  }
};

//controller phân trang

module.exports = {
  getUsers,
  getUserById,
  postUser,
  updateUser,
  deleteUser,
  getUserByName,
};
