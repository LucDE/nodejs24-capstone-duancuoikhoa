const { PrismaClient } = require("@prisma/client");
const { successCode, failCode, errorCode } = require("../ultis/response");
const prisma = new PrismaClient();

const getChiTietLoaiCongViec = async (req, res) => {
  try {
    let result = await prisma.chiTietLoaiCongViec.findMany();
    successCode(res, result, "Lấy danh sách công việc thành công !");
  } catch (err) {
    errorCode(res, "Lấy danh sách thất bại !");
  }
};

const addChiTietLoaiCongViec = async (req, res) => {
  try {
    let { ten_chi_tiet, hinh_anh, ma_loai_cong_viec } = req.body;

    let checkMaLoaiCongViec = await prisma.loaiCongViec.findFirst({
      where: {
        id: Number(ma_loai_cong_viec),
      },
    });
    if (!checkMaLoaiCongViec) {
      failCode(res, ma_loai_cong_viec, "Không tồn tại mã loại công việc");
      return;
    }
    let data = {
      ten_chi_tiet,
      hinh_anh,
      ma_loai_cong_viec,
    };
    let result = await prisma.chiTietLoaiCongViec.create({ data });
    successCode(res, result, "Thêm chi tiết loại công việc thành công!");
  } catch (err) {
    console.log("err: ", err);
    errorCode(res, "Lỗi BE");
  }
};

const getChiTietLoaiCongViecById = async (req, res) => {
  try {
    let { id } = req.params;
    let result = await prisma.chiTietLoaiCongViec.findFirst({
      where: {
        id: Number(id),
      },
    });
    if (result) {
      successCode(res, result, "Lấy chi tiết loại công việc thành công !");
    } else {
      failCode(res, result, "Không tìm thấy chi tiết loại công việc !");
    }
  } catch (err) {
    errorCode(res, "Lỗi BE");
  }
};

const updateChiTietLoaiCongViec = async (req, res) => {
  try {
    let { id, ten_chi_tiet, hinh_anh, ma_loai_cong_viec } = req.body;
    let checkId = await prisma.chiTietLoaiCongViec.findFirst({
      where: {
        id: Number(id),
      },
    });
    if (!checkId) {
      failCode(res, id, "Không tìm thấy chi tiết loại công việc !");
      return;
    } else {
      let checkMaLoaiCongViec = await prisma.loaiCongViec.findFirst({
        where: {
          id: Number(ma_loai_cong_viec),
        },
      });
      if (!checkMaLoaiCongViec) {
        failCode(res, ma_loai_cong_viec, "Không tồn tại loại công việc");
        return;
      }
      let data = {
        id,
        ten_chi_tiet,
        hinh_anh,
        ma_loai_cong_viec,
      };
      let result = await prisma.chiTietLoaiCongViec.update({
        data,
        where: {
          id,
        },
      });
      successCode(res, result, "Update chi tiết công việc thành công !");
    }
  } catch (err) {
    console.log("err: ", err);
    errorCode(res, "Lỗi BE");
  }
};

const deleteChiTietLoaiCongViec = async (req, res) => {
  try {
    let { id } = req.params;
    let checkId = await prisma.chiTietLoaiCongViec.findFirst({
      where: {
        id: Number(id),
      },
    });
    if (!checkId) {
      failCode(res, id, "Không tìm thấy chi tiết loại công việc cần xóa !");
      return;
    }
    let result = await prisma.chiTietLoaiCongViec.delete({
      where: { id: Number(id) },
    });
    successCode(res, result, "Xóa thành công !");
  } catch (err) {
    console.log("err: ", err);
    errorCode(res, "Lỗi BE");
  }
};

const getChiTietLoaiCongViecPhanTrang = async (req, res) => {
  try {
    let { pageIndex, pageSize } = req.params;
    let danhSachChiTietLoaiCongViec = await prisma.chiTietLoaiCongViec.count();
    let totalPage = Math.ceil(danhSachChiTietLoaiCongViec / pageSize);
    const result = await prisma.chiTietLoaiCongViec.findMany({
      skip: +pageSize * +pageIndex - +pageSize,
      take: +pageSize,
    });
    successCode(res, { totalPage, dsChiTietLoai: result }, "Lấy ds thành công");
  } catch (err) {
    errorCode(res, "Lỗi BE");
  }
};

module.exports = {
  getChiTietLoaiCongViec,
  addChiTietLoaiCongViec,
  getChiTietLoaiCongViecById,
  updateChiTietLoaiCongViec,
  deleteChiTietLoaiCongViec,
  getChiTietLoaiCongViecPhanTrang,
};
