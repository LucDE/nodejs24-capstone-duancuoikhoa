const { PrismaClient } = require("@prisma/client");
const { successCode, failCode, errorCode } = require("../ultis/response");
const prisma = new PrismaClient();

const getBinhLuan = async (req, res) => {
  try {
    let result = await prisma.binhLuan.findMany();
    successCode(res, result, "Lấy danh sách bình luận thành công !");
  } catch (err) {
    errorCode(res, "Lấy danh sách thất bại !");
  }
};

const postBinhLuan = async (req, res) => {
  try {
    let { ma_cong_viec, ma_nguoi_binh_luan, noi_dung, sao_binh_luan } =
      req.body;
    let checkCongViec = await prisma.congViec.findFirst({
      where: {
        id: Number(ma_cong_viec),
      },
    });
    let checkNguoiBinhLuan = await prisma.nguoiDung.findFirst({
      where: {
        id: Number(ma_nguoi_binh_luan),
      },
    });
    if (!checkNguoiBinhLuan) {
      failCode(res, ma_nguoi_binh_luan, "Không tồn tại người bình luận");
      return;
    }
    if (!checkCongViec) {
      failCode(res, ma_cong_viec, "Không tồn tại công việc");
      return;
    }
    let data = {
      ma_cong_viec,
      ma_nguoi_binh_luan,
      ngay_binh_luan: new Date(),
      noi_dung,
      sao_binh_luan,
    };
    let result = await prisma.binhLuan.create({ data });
    successCode(res, result, "Thêm bình luận thành công!");
  } catch (err) {
    console.log("err: ", err);
    errorCode(res, "Lỗi BE");
  }
};

const getBinhLuanById = async (req, res) => {
  try {
    let { id } = req.params;
    let result = await prisma.binhLuan.findFirst({
      where: {
        id: Number(id),
      },
    });
    if (result) {
      successCode(res, result, "Lấy bình luận thành công !");
    } else {
      failCode(res, result, "Không tìm thấy bình luận !");
    }
  } catch (err) {
    errorCode(res, "Lỗi BE");
  }
};

const updateBinhLuan = async (req, res) => {
  try {
    let { id, ma_cong_viec, ma_nguoi_binh_luan, noi_dung, sao_binh_luan } =
      req.body;
    let checkId = await prisma.binhLuan.findFirst({
      where: {
        id: Number(id),
      },
    });
    if (!checkId) {
      failCode(res, id, "Không tìm thấy bình luận !");
      return;
    } else {
      let checkCongViec = await prisma.congViec.findFirst({
        where: {
          id: Number(ma_cong_viec),
        },
      });
      let checkNguoiBinhLuan = await prisma.nguoiDung.findFirst({
        where: {
          id: Number(ma_nguoi_binh_luan),
        },
      });
      if (!checkNguoiBinhLuan) {
        failCode(res, ma_nguoi_binh_luan, "Không tồn tại người bình luận");
        return;
      }
      if (!checkCongViec) {
        failCode(res, ma_cong_viec, "Không tồn tại công việc");
        return;
      }
      let data = {
        id,
        ma_cong_viec,
        ma_nguoi_binh_luan,
        ngay_binh_luan: new Date(),
        noi_dung,
        sao_binh_luan,
      };
      let result = await prisma.binhLuan.update({
        data,
        where: {
          id,
        },
      });
      successCode(res, result, "Update bình luận thành công !");
    }
  } catch (err) {
    console.log("err: ", err);
    errorCode(res, "Lỗi BE");
  }
};

const deleteBinhLuan = async (req, res) => {
  try {
    let { id } = req.params;
    let checkId = await prisma.binhLuan.findFirst({
      where: {
        id: Number(id),
      },
    });
    if (!checkId) {
      failCode(res, id, "Không tìm thấy bình luận cần xóa !");
      return;
    }
    let result = await prisma.binhLuan.delete({
      where: { id: Number(id) },
    });
    successCode(res, result, "Xóa thành công !");
  } catch (err) {
    console.log("err: ", err);
    errorCode(res, "Lỗi BE");
  }
};

const getBinhLuanTheoMaCongViec = async (req, res) => {
  try {
    let { maCongViec } = req.params;
    let result = await prisma.binhLuan.findFirst({
      where: {
        ma_cong_viec: Number(maCongViec),
      },
    });
    if (!result) {
      failCode(res, result, "Không tìm thấy mã công việc !");
    }
    successCode(res, result, "Lấy bình luận thành công !");
  } catch (err) {
    console.log("err: ", err);
    errorCode(res, "Lỗi BE");
  }
};

module.exports = {
  getBinhLuan,
  getBinhLuanById,
  postBinhLuan,
  updateBinhLuan,
  deleteBinhLuan,
  getBinhLuanTheoMaCongViec,
};
