const { PrismaClient } = require("@prisma/client");
const { successCode, failCode, errorCode } = require("../ultis/response");
const prisma = new PrismaClient();

const getLoaiCongViec = async (req, res) => {
  try {
    let result = await prisma.loaiCongViec.findMany();
    successCode(res, result, "Lấy danh sách loại công việc thành công !");
  } catch (err) {
    errorCode(res, "Lấy danh sách thất bại !");
  }
};

const postLoaiCongViec = async (req, res) => {
  try {
    let { ten_loai_cong_viec } = req.body;
    let data = {
      ten_loai_cong_viec,
    };
    let result = await prisma.loaiCongViec.create({ data });
    successCode(res, result, "Thêm loại công việc thành công!");
  } catch (err) {
    console.log("err: ", err);
    errorCode(res, "Lỗi BE");
  }
};

const getLoaiCongViecById = async (req, res) => {
  try {
    let { id } = req.params;
    let result = await prisma.loaiCongViec.findFirst({
      where: {
        id: Number(id),
      },
    });
    if (result) {
      successCode(res, result, "Lấy loại công việc thành công !");
    } else {
      failCode(res, result, "Không tìm thấy loại công việc !");
    }
  } catch (err) {
    errorCode(res, "Lỗi BE");
  }
};

const updateLoaiCongViec = async (req, res) => {
  try {
    let { id, ten_loai_cong_viec } = req.body;
    let checkId = await prisma.loaiCongViec.findFirst({
      where: {
        id: Number(id),
      },
    });
    if (!checkId) {
      failCode(res, id, "Không tìm thấy loại công việc !");
      return;
    } else {
      let data = {
        id,
        ten_loai_cong_viec,
      };
      let result = await prisma.loaiCongViec.update({
        data,
        where: {
          id,
        },
      });
      successCode(res, result, "Update loại công việc thành công !");
    }
  } catch (err) {
    console.log("err: ", err);
    errorCode(res, "Lỗi BE");
  }
};

const deleteLoaiCongViec = async (req, res) => {
  try {
    let { id } = req.params;
    let checkId = await prisma.loaiCongViec.findFirst({
      where: {
        id: Number(id),
      },
    });
    if (!checkId) {
      failCode(res, id, "Không tìm thấy loại công việc cần xóa !");
      return;
    }
    let result = await prisma.loaiCongViec.delete({
      where: { id: Number(id) },
    });
    successCode(res, result, "Xóa thành công !");
  } catch (err) {
    console.log("err: ", err);
    errorCode(res, "Lỗi BE");
  }
};

module.exports = {
  getLoaiCongViec,
  postLoaiCongViec,
  getLoaiCongViecById,
  updateLoaiCongViec,
  deleteLoaiCongViec,
};
