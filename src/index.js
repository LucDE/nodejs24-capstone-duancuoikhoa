const express = require("express");
const port = 8080;
const cors = require("cors");
const rootRoute = require("./routes/index");

const app = express();

app.use(express.json());
app.use(express.static("."));
app.use(cors());
app.use("/api", rootRoute);

app.listen(port, () => {
  console.log(`App is listening at port ${port}`);
});
